#!/bin/bash

BLACK=30
RED=31
GREEN=32
YELLOW=33
BLUE=34
MAGENTA=35
CYAN=36
WHITE=97

GRAY=90
LIGHT_RED=91
LIGHT_GREEN=92
LIGHT_YELLOW=93
LIGHT_BLUE=94
LIGHT_MAGENTA=95
LIGHT_CYAN=96
LIGHT_GRAY=37

function color {
    fg=$1
    text=$2
    echo -e "\e[${fg}m${text}\e[0m"
}

function background {
    bg=$(( $1 + 10 ))
    text=$2
    echo -e "\e[${bg}m${text}\e[0m"
}

function bold {
    fg=$1
    text=$2
    echo -e "\e[1;${fg}m${text}\e[0m"
}

function italic {
    fg=$1
    text=$2
    echo -e "\e[3;${fg}m${text}\e[0m"
}
